// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTank.h"
#include "TankPlayerController.h"

void ATankPlayerController::BeginPlay() {
	Super::BeginPlay();

	auto ControlledTank = GetControlledTank();
	if (ControlledTank) {
		UE_LOG(LogTemp, Warning, TEXT("PlayerController controls tank: %s"), *(ControlledTank->GetFName().ToString()));
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("PlayerController controls no tank"));
	}
}

void ATankPlayerController::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	AimTowardsCrosshair();
}

ATank * ATankPlayerController::GetControlledTank() const {
	return Cast<ATank>(GetPawn());
}

void ATankPlayerController::AimTowardsCrosshair() {
	ATank *ControlledTank = GetControlledTank();
	if (!ControlledTank) { return; }

	FVector HitLocation;
	if (GetSightRayHitLocation(HitLocation)) {
		ControlledTank->AimAt(HitLocation);
	}
}

bool ATankPlayerController::GetSightRayHitLocation(FVector& OutHitLocation) const {
	int32 ScreenSizeX, ScreenSizeY;
	GetViewportSize(ScreenSizeX, ScreenSizeY);
	FVector2D ScreenLocation(ScreenSizeX * CrosshairX, ScreenSizeY * CrosshairY);

	FVector LookDirection;
	if (GetLookDirection(ScreenLocation, LookDirection)) {
		return GetLookVectorHitLocation(OutHitLocation, LookDirection);
	}
	else { 
		OutHitLocation = FVector(FVector::ZeroVector);
		return false; 
	}
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector & OutLookHitLocation, const FVector Direction) const {
	FHitResult HitResult;
	FVector Start = PlayerCameraManager->GetCameraLocation();

	if (
		GetWorld()->LineTraceSingleByChannel(
			HitResult,
			Start,
			Start + Direction * LineTraceRange,
			ECollisionChannel::ECC_Visibility
		)
	) {
		OutLookHitLocation = HitResult.Location;
		return true;
	}
	else {
		OutLookHitLocation = FVector(FVector::ZeroVector);
		return false;
	}
}


bool ATankPlayerController::GetLookDirection(FVector2D ScreenLocation, FVector & LookDirection) const {
	FVector CameraWorldLocation;
	return DeprojectScreenPositionToWorld(
		ScreenLocation.X, 
		ScreenLocation.Y, 
		CameraWorldLocation,
		LookDirection
	);
}

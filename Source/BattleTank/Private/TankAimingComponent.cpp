// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTank.h"
#include "Tank.h"
#include "TankAimingComponent.h"


// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();	
}


// Called every frame
void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

ATank * UTankAimingComponent::GetTank() {
	return Cast<ATank>(GetOwner());
}

void UTankAimingComponent::AimAt(FVector AimAtLocation, float ProjectileSpeed) {
	if (!Barrel) { return; }

	FVector OutLaunchVelocity;
	FVector Start = Barrel->GetSocketLocation(BarrelEndSocketName);

	if (UGameplayStatics::SuggestProjectileVelocity(
			this,
			OutLaunchVelocity,
			Start,
			AimAtLocation,
			ProjectileSpeed,
			false,
			0,
			0,
			ESuggestProjVelocityTraceOption::DoNotTrace
	)) {
		FVector AimDirection = OutLaunchVelocity.GetSafeNormal();
		UE_LOG(
			LogTemp,
			Warning,
			TEXT("%s Aiming at %s"),
			*GetOwner()->GetName(),
			*AimDirection.ToString()
			//*AimAtLocation.ToString()
		);
		MoveBarrel(AimDirection);
	}
}

void UTankAimingComponent::SetBarrel(UStaticMeshComponent *BarrelToSet) {
	this->Barrel = BarrelToSet;
}

void UTankAimingComponent::MoveBarrel(FVector DesiredDirection) {
	// Get Barrel Mesh
	// Get current barrel orientation
	// Cross product barrel orientation x desired orientation
	//// If 0, return
	//// Otherwise, rotate barrel by (BarrelOrientation dot DesiredOrientation)

	if (!Barrel) { return; }

	UStaticMeshComponent* Turret = Cast<UStaticMeshComponent>(Barrel->GetAttachParent());	
	if (!Turret) { return; }

	FRotator TurretRotation = Turret->GetComponentRotation();
	FRotator BarrelRotation = Barrel->GetForwardVector().Rotation();

	FRotator DesiredRotation = DesiredDirection.Rotation();

	FRotator BarrelDeltaRotation = DesiredRotation - BarrelRotation;
	BarrelDeltaRotation.Yaw = 0;

	FRotator TurretDeltaRotation = DesiredRotation - TurretRotation;
	TurretDeltaRotation.Pitch = 0;

	Turret->AddLocalRotation(TurretDeltaRotation);
	Barrel->AddLocalRotation(BarrelDeltaRotation);

	UE_LOG(LogTemp, Warning, TEXT("Setting rotation to %s"), *DesiredRotation.ToString());
}
// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleTank.h"
#include "TankAIController.h"

ATank* ATankAIController::GetControlledTank() const {
	return Cast<ATank>(GetPawn());
}

ATank * ATankAIController::GetPlayerTank() const {
	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	if (PlayerPawn) {
		return Cast<ATank>(PlayerPawn);
	}
	else {
		return nullptr;
	}
}

void ATankAIController::BeginPlay() { 
	Super::BeginPlay();

	auto PlayerTank = GetPlayerTank();

	if (PlayerTank) {
		UE_LOG(LogTemp, Warning, TEXT("AIController found player tank: %s"), *PlayerTank->GetFName().ToString());
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("AIController found no player tank"));
	}
}

void ATankAIController::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	ATank *PlayerTank = GetPlayerTank();
	ATank *ControlledTank = GetControlledTank();

	if (PlayerTank && ControlledTank) { ControlledTank->AimAt(PlayerTank->GetActorLocation()); }
}

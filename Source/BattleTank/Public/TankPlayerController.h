// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Tank.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	float CrosshairX = 0.5;
	
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	float CrosshairY = 0.3333;

	UPROPERTY(EditAnywhere)
	float LineTraceRange = 10e+5;

private:
	ATank * GetControlledTank() const;
	void AimTowardsCrosshair();
	bool GetSightRayHitLocation(FVector & OutHitLocation) const;
	bool GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const;
	bool GetLookVectorHitLocation(FVector & OutLookHitLocation, const FVector Direction) const;
	
	void BeginPlay() override;
	void Tick(float delta) override;
};
